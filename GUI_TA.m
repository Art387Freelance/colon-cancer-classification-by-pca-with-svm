function varargout = GUI_TA(varargin)
% GUI_TA MATLAB code for GUI_TA.fig
%      GUI_TA, by itself, creates a new GUI_TA or raises the existing
%      singleton*.
%
%      H = GUI_TA returns the handle to a new GUI_TA or the handle to
%      the existing singleton*.
%
%      GUI_TA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TA.M with the given input arguments.
%
%      GUI_TA('Property','Value',...) creates a new GUI_TA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_TA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_TA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_TA

% Last Modified by GUIDE v2.5 10-Jul-2019 00:21:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_TA_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_TA_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_TA is made visible.
function GUI_TA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_TA (see VARARGIN)

% Choose default command line output for GUI_TA
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes GUI_TA wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_TA_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;





function editEntropy_Callback(hObject, eventdata, handles)
% hObject    handle to editEntropy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEntropy as text
%        str2double(get(hObject,'String')) returns contents of editEntropy as a double


% --- Executes during object creation, after setting all properties.
function editEntropy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEntropy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMean_Callback(hObject, eventdata, handles)
% hObject    handle to editMean (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMean as text
%        str2double(get(hObject,'String')) returns contents of editMean as a double


% --- Executes during object creation, after setting all properties.
function editMean_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMean (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editSkewness_Callback(hObject, eventdata, handles)
% hObject    handle to editSkewness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSkewness as text
%        str2double(get(hObject,'String')) returns contents of editSkewness as a double


% --- Executes during object creation, after setting all properties.
function editSkewness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSkewness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function editName_Callback(hObject, eventdata, handles)
% hObject    handle to editName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editName as text
%        str2double(get(hObject,'String')) returns contents of editName as a double


% --- Executes during object creation, after setting all properties.
function editName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in identifikasi.
function identifikasi_Callback(hObject, eventdata, handles)
% hObject    handle to identifikasi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ciri_uji
load hasil_training_svm 
id_kelas_uji = predict(Mdl, ciri_uji);

if id_kelas_uji == 1
    hasil = 'Merupakan Kelas Carcinoma';
elseif id_kelas_uji == 2
    hasil = 'Merupakan Kelas Lymphoma';
elseif id_kelas_uji == 3
    hasil = 'Merupakan Kelas Normal';
end

set(handles.textIdentifikasi,'String', hasil);








% --- Executes on button press in extraction.
function extraction_Callback(hObject, eventdata, handles)
% hObject    handle to extraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global citra_gray ciri_uji
% [baris, kolom, layer] = size(gray);

[coeff,score,latent] = pca(double(citra_gray));
ciri_mean = mean(latent);
ciri_var = var(latent);
ciri_std = std(latent);
ciri_skew = skewness(latent);
ciri_kurt = kurtosis(latent);
ciri_ent = entropy(latent);

ciri_uji = [ciri_ent ciri_mean ciri_skew];

set(handles.editMean,'string', ciri_mean);
set(handles.editEntropy,'string', ciri_ent);
set(handles.editSkewness,'string', ciri_skew);






% --- Executes on button press in preproc.
function preproc_Callback(hObject, eventdata, handles)
% hObject    handle to preproc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img citra_gray
citra = img;
citra = imresize(citra,[64 64]);
citra_gray = rgb2gray(citra);
citra_gray = citra(:,:,2);

axes(handles.axes4);
imshow(citra_gray);


% --- Executes on button press in browse.
function browse_Callback(hObject, eventdata, handles)
% hObject    handle to browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global img
[filename,pathname] = uigetfile('*.jpg');
bacaimage = fullfile(pathname,filename);
img = imread(bacaimage);
axes(handles.axes3);
imshow(img);
set(handles.editName,'string',filename);
