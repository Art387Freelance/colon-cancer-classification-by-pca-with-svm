clear
close all
clc

tic
load hasil_training_svm

dname = uigetdir();
isi_folder=dir(dname);
jml_uji = 0;
jml_benar = 0;
%db_ciri_uji = [];
for i=3:length(isi_folder)
    nama_folder=isi_folder(i).name;
    isi_file=dir([dname,'\',nama_folder,'\*.jpg']);
    for j=1:length(isi_file)
        jml_uji = jml_uji+1;
        nama_file = isi_file(j).name;
        alamat_file = [dname,'\',nama_folder,'\',nama_file];
        citra = imread(alamat_file);
        citra = imresize(citra,[64 64]);
        citra_gray = rgb2gray(citra);
        citra_gray = citra(:,:,2);
        [baris, kolom, layer] = size(gray);
        [coeff,score,latent] = pca(double(citra_gray));
        ciri_mean = mean(latent);
        ciri_var = var(latent);
        ciri_std = std(latent);
        ciri_skew = skewness(latent);
        ciri_kurt = kurtosis(latent);
        ciri_ent = entropy(latent);
        %ciri_uji = [ciri_mean ciri_var ciri_std ciri_skew ciri_kurt ciri_ent];
        ciri_uji = [ciri_ent ciri_mean ciri_skew];
%         ciri_uji = [ciri_uji; ciri_uji];
        id_kelas_uji = predict(Mdl, ciri_uji);
        kelas_uji = nama_kelas{id_kelas_uji};
        if strcmpi(kelas_uji,nama_folder)
            jml_benar = jml_benar+1;
            disp([num2str(jml_uji),'. ',nama_file,' ',nama_folder,' ',kelas_uji,' = benar']);
        else
            disp([num2str(jml_uji),'. ',nama_file,' ',nama_folder,' ',kelas_uji,' = salah']);
        end
        
    end
end

toc
akurasi = jml_benar ./ jml_uji .* 100