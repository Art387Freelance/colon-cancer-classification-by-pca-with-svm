clear
close all
clc

load data_ciri_latih

t = templateSVM('KernelFunction','rbf'); %rbf ridial basis function, linear kalau bisa cari yg multi class gaussian linier
Mdl = fitcecoc(ciri_latih,target,'Learners',t,'Coding','onevsall'); %onevsall

save hasil_training_svm