clear
close all
clc

tic
dname = uigetdir();
isi_folder=dir(dname);
nama_kelas = {};
ciri_latih = [];
target = [];
for i=3:length(isi_folder)
    nama_folder=isi_folder(i).name;
    isi_file=dir([dname,'\',nama_folder,'\*.jpg']);
    nama_kelas{i-2} = nama_folder;
    for j=1:length(isi_file)
        nama_file = isi_file(j).name;
        alamat_file = [dname,'\',nama_folder,'\',nama_file];
        citra = imread(alamat_file);
        citra = imresize(citra,[64 64]);
        citra_gray = rgb2gray(citra);
        citra_gray = citra(:,:,2);
        [baris, kolom, layer] = size(gray);
        [coeff,score,latent] = pca(double(citra_gray));
        ciri_mean = mean(latent);
        ciri_var = var(latent);
        ciri_std = std(latent);
        ciri_skew = skewness(latent);
        ciri_kurt = kurtosis(latent);
        ciri_ent = entropy(latent);
        %ciri_stats = [ciri_mean ciri_var ciri_std ciri_skew ciri_kurt ciri_ent];
        ciri_stats = [ciri_ent ciri_mean ciri_skew];
        ciri_latih = [ciri_latih; ciri_stats];
        target = [target; i-2];
    end
end

toc
save data_ciri_latih ciri_latih target nama_kelas